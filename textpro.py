def sentence_maker(phrase):
    interrogates = ('how', 'what', 'why')
    capitalized = phrase.capitalize()
    if phrase.startswith(interrogates):
        return "{}?".format(capitalized)
    else:
        return "{}.".format(capitalized)


def read_input():
    inputs = []
    while True:
        user_input = input("Say somthing:")
        if user_input == "\end":
            break
        else:
            inputs.append(sentence_maker(user_input))
    return inputs


if __name__ == '__main__':
    results = read_input()
    print(" ".join(results))
